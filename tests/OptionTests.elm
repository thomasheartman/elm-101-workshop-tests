module OptionTests exposing (functor, monad, monoid, withDefault)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, float, int, list, maybe, percentage)
import Option exposing (..)
import Test exposing (..)


option =
    Fuzz.map fromMaybe << maybe


pair =
    Fuzz.map (\mb -> ( fromMaybe mb, mb )) << maybe


toMaybe o =
    case o of
        Some x ->
            Just x

        None ->
            Nothing


fromMaybe m =
    case m of
        Just x ->
            Some x

        Nothing ->
            None


withDefault : Test
withDefault =
    fuzz2 (pair int) int "withDefault matches the standard implementation" <|
        \( opt, mb ) default ->
            Option.withDefault default opt
                |> Expect.equal (Maybe.withDefault default mb)


monoid : Test
monoid =
    let
        mappendF =
            mappend (+)
    in
    describe "Monoid functionality"
        [ fuzz (option int) "appending mempty should return the original" <|
            \x ->
                Expect.all
                    [ Expect.equal (mappendF x mempty)
                    , Expect.equal (mappendF mempty x)
                    ]
                    x
        , fuzz3 (option int) (option int) (option int) "mappending is associative" <|
            \o p q ->
                mappendF (mappendF o p) q
                    |> Expect.equal (mappendF o (mappendF p q))
        ]


functor : Test
functor =
    describe "Functor functionality"
        [ fuzz (pair int) "mapping a doubler works" <|
            \( opt, mb ) ->
                let
                    f =
                        (*) 2
                in
                fmap f opt
                    |> Expect.equal (Maybe.map f mb |> fromMaybe)
        ]


monad : Test
monad =
    describe "Monad functions"
        [ fuzz int "return works as expected" <|
            \x -> Expect.equal (return x) (Just x |> fromMaybe)
        , fuzz (pair int) "binding works" <|
            \( opt, mb ) ->
                bind (return << identity) opt
                    |> Expect.equal (Maybe.andThen Just mb |> fromMaybe)
        ]
