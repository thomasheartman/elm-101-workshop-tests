module ZipListTests exposing (getCurrentElement, higherOrderFunctions, listRepresentation, reversing, zipping)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, float, int, list, percentage)
import Test exposing (..)
import ZipList as ZipList exposing (..)


fuzzElements =
    fuzz3 (list int) int (list int)


fuzzListAndElement =
    fuzz2 (list int) int


zipList fuzzer =
    Fuzz.map3 new (list fuzzer) fuzzer (list fuzzer)


listRepresentation : Test
listRepresentation =
    describe "List interop"
        [ fuzzElements "It creates correct list representations" <|
            \prev curr next ->
                let
                    result =
                        toList <| new prev curr next
                in
                Expect.equal (prev ++ [ curr ] ++ next) result
        , fuzz (list int) "it can be created from a list if there is at least one element" <|
            \list ->
                case fromList list of
                    Nothing ->
                        Expect.equal [] list

                    Just _ ->
                        Expect.notEqual [] list
        , fuzz (list int) "the head of the list becomes the current element" <|
            \list ->
                let
                    curr =
                        fromList list
                            |> Maybe.map current
                in
                case list of
                    [] ->
                        Expect.equal Nothing curr

                    x :: xs ->
                        Expect.equal (Just x) curr
        ]


getCurrentElement : Test
getCurrentElement =
    describe "current functionality"
        [ fuzzElements "Current returns the current value" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next
                in
                Expect.equal curr (current list)
        , fuzzElements "Current returns the correct value after stepping forward" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next

                    expected =
                        case next of
                            [] ->
                                curr

                            x :: xs ->
                                x
                in
                Expect.equal expected (forward list |> current)
        , fuzzElements "Current returns the correct value after stepping backward" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next

                    expected =
                        case List.reverse prev of
                            [] ->
                                curr

                            x :: xs ->
                                x
                in
                Expect.equal expected (backward list |> current)
        ]


reversing : Test
reversing =
    describe "Reversing"
        [ fuzzElements "twice gives the original order" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next
                in
                list
                    |> reverse
                    |> reverse
                    |> Expect.equal list
        , fuzzElements "gives the expected order" <|
            \prev curr next ->
                new prev curr next
                    |> reverse
                    |> toList
                    |> Expect.equal (List.reverse <| prev ++ [ curr ] ++ next)
        ]


zipping : Test
zipping =
    describe "Zipping"
        [ describe "General"
            [ fuzz int "it handles a single element" <|
                \x ->
                    let
                        list =
                            new [] x []
                    in
                    Expect.all
                        [ Expect.equal <| forward list
                        , Expect.equal <| backward list
                        ]
                        list
            , fuzzListAndElement "it handles empty previous list" <|
                \l x ->
                    let
                        list =
                            new [] x l
                    in
                    Expect.equal list (backward list)
            , fuzzListAndElement "it handles empty next list" <|
                \l x ->
                    let
                        list =
                            new l x []
                    in
                    Expect.equal list (forward list)
            ]
        , describe "It steps backward"
            [ test "with two populated lists" <|
                \_ ->
                    let
                        list =
                            new [ 1, 2 ] 3 [ 4, 5 ]

                        expected =
                            new [ 1 ] 2 [ 3, 4, 5 ]
                    in
                    Expect.equal expected (backward list)
            , test "into empty list" <|
                \_ ->
                    let
                        list =
                            new [ 1 ] 2 [ 3, 4 ]

                        expected =
                            new [] 1 [ 2, 3, 4 ]
                    in
                    Expect.equal expected (backward list)
            ]
        , describe "Forward motion"
            [ test "it steps forward" <|
                \_ ->
                    let
                        list =
                            new [ 1 ] 2 [ 3, 4 ]

                        expected =
                            new [ 1, 2 ] 3 [ 4 ]
                    in
                    Expect.equal expected (forward list)
            , test "into empty list" <|
                \_ ->
                    let
                        list =
                            new [ 1, 2 ] 3 [ 4 ]

                        expected =
                            new [ 1, 2, 3 ] 4 []
                    in
                    Expect.equal expected (forward list)
            ]
        , let
            positiveInt =
                Fuzz.map abs <| int

            negativeInt =
                Fuzz.map (abs >> (-) 0) <| int
          in
          describe "Multi-step functionality"
            [ fuzz (zipList int) "rewind brings you back to start" <|
                rewind
                    >> start
                    >> Expect.true "We're at the first element now"
            , fuzz (zipList int) "fast forward brings you back to start" <|
                fastForward
                    >> end
                    >> Expect.true "We're at the last element"
            , fuzz3 (list int) int positiveInt "Test step forward functionality" <|
                \list curr steps ->
                    let
                        z =
                            new [] curr list |> stepForward steps

                        remaining =
                            max 0 <| List.length list - steps
                    in
                    case remaining of
                        0 ->
                            Expect.true "We're at the end of the list" (end z)

                        _ ->
                            Expect.false "We're not at the end of the list" (end z)
            , fuzz2 (zipList int) negativeInt "Stepping forward (-x) should be the same as stepping backward x" <|
                \list steps ->
                    stepForward steps list
                        |> Expect.equal (stepBackward (abs steps) list)
            , fuzz3 (list int) int positiveInt "Test step backward functionality" <|
                \list curr steps ->
                    let
                        z =
                            new list curr [] |> stepBackward steps

                        remaining =
                            max 0 <| List.length list - steps
                    in
                    case remaining of
                        0 ->
                            Expect.true "We're at the start of the list" (start z)

                        _ ->
                            Expect.false "We're not at the start of the list" (start z)
            , fuzz2 (zipList int) negativeInt "Stepping backward (-x) should be the same as stepping forward x" <|
                \list steps ->
                    stepBackward steps list
                        |> Expect.equal (stepForward (abs steps) list)
            ]
        , describe "Start and end booleans" <|
            [ fuzzElements "It's at the start if there are no previous elements" <|
                \prev curr next ->
                    let
                        list =
                            new prev curr next
                    in
                    case prev of
                        [] ->
                            Expect.true "The list should be at the start if there are no previous elements" (start list)

                        x :: xs ->
                            Expect.false "The list should not be at the start if there are previous elements" (start list)
            , fuzzElements "It's at the end if there are no next elements" <|
                \prev curr next ->
                    let
                        list =
                            new prev curr next
                    in
                    case next of
                        [] ->
                            Expect.true "The list should be at the end if there are no next elements" (end list)

                        x :: xs ->
                            Expect.false "The list should not be at the end if there are next elements" (end list)
            ]
        ]


higherOrderFunctions : Test
higherOrderFunctions =
    describe "Higher order functions"
        [ fuzzListAndElement "It maps" <|
            \l x ->
                let
                    list =
                        new [] x l

                    func =
                        (+) 1
                in
                map func list
                    |> Expect.equal (new [] (func x) (List.map func l))
        , fuzzListAndElement "it folds right" <|
            \l x ->
                let
                    list =
                        new [] x l

                    func =
                        (-)

                    initial =
                        0
                in
                foldr func initial list
                    |> Expect.equal (List.foldr func initial (x :: l))
        , fuzzListAndElement "it folds left" <|
            \l x ->
                let
                    list =
                        new [] x l

                    func =
                        (-)

                    initial =
                        0
                in
                foldl func initial list
                    |> Expect.equal (List.foldl func initial (x :: l))
        ]
