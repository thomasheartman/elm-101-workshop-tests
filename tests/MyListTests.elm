module MyListTests exposing (functor, listBehavior, listConversions, monad, monoid)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, float, int, list, percentage)
import MyList exposing (..)
import Test exposing (..)


myList =
    Fuzz.map fromList << list


pair =
    Fuzz.map (\x -> ( fromList x, x )) << list


mapConvert f =
    List.map f >> fromList


listConversions : Test
listConversions =
    let
        testList =
            [ 1, 2, 3 ]

        testMyList =
            Cons 1 <| Cons 2 <| Cons 3 <| Nil
    in
    describe "Converting to and from lists"
        [ test "Converting from a list gives the expected result" <|
            \_ ->
                fromList testList
                    |> Expect.equal testMyList
        , test "Converting to a lst gives the expected result" <|
            \_ ->
                toList testMyList
                    |> Expect.equal testList
        , fuzz (list int) "There and back again gets us back to start" <|
            \xs ->
                xs
                    |> fromList
                    |> toList
                    |> Expect.equal xs
        , fuzz (myList int) "There and back again from MyList" <|
            \xs ->
                xs
                    |> toList
                    |> fromList
                    |> Expect.equal xs
        ]


monoid : Test
monoid =
    describe "Monoid functionality"
        [ fuzz (myList int) "appending mempty should return the original" <|
            \l ->
                Expect.all
                    [ Expect.equal (mappend l mempty)
                    , Expect.equal (mappend mempty l)
                    ]
                    l
        , fuzz2 (pair int) (pair int) "mappending two lists works" <|
            \( l, l_ ) ( m, m_ ) ->
                mappend l m
                    |> Expect.equal (l_ ++ m_ |> fromList)
        , fuzz3 (myList int) (myList int) (myList int) "mappending is associative" <|
            \l m n ->
                mappend (mappend l m) n
                    |> Expect.equal (mappend l (mappend m n))
        ]


functor : Test
functor =
    describe "Functor functionality"
        [ fuzz (pair int) "mapping a doubler works" <|
            \( l, l_ ) ->
                fmap ((*) 2) l
                    |> Expect.equal (mapConvert ((*) 2) l_)
        , fuzz2 (pair int) int "always mapping works" <|
            \( l, l_ ) x ->
                fmapAlways x l
                    |> Expect.equal (mapConvert (always x) l_)
        ]


monad : Test
monad =
    describe "Monad functionality"
        [ fuzz int "return wraps the element in a list" <|
            \x ->
                return x
                    |> Expect.equal ([ x ] |> fromList)
        , fuzz (pair int) "bind works the same way as it does on lists" <|
            \( l, l_ ) ->
                let
                    f =
                        List.repeat 3
                in
                bind (f >> fromList) l
                    |> Expect.equal (List.concatMap f l_ |> fromList)
        ]


listBehavior : Test
listBehavior =
    describe "standard list functions" <|
        [ describe "filter"
            [ fuzz (myList int) "empties list if always false" <|
                \l ->
                    filter (always False) l
                        |> Expect.equal Nil
            , fuzz (myList int) "returns same list if always True" <|
                \l ->
                    filter (always True) l
                        |> Expect.equal l
            ]
        , describe "map"
            [ fuzz (pair int) "map matches the regular implementation" <|
                \( l, l_ ) ->
                    let
                        f =
                            always True
                    in
                    map f l
                        |> Expect.equal (mapConvert f l_)
            ]
        , describe "length"
            [ fuzz (pair int) "length matches the regular implementation" <|
                \( l, l_ ) ->
                    length l
                        |> Expect.equal (List.length l_)
            ]
        , describe "member"
            [ fuzz2 (pair int) int "member works the same as for list" <|
                \( l, l_ ) needle ->
                    member needle l
                        |> Expect.equal (List.member needle l_)
            ]
        , describe "take & drop"
            [ fuzz2 (pair int) int "taking matches the list implementation" <|
                \( l, l_ ) n ->
                    take n l
                        |> Expect.equal (List.take n l_ |> fromList)
            , fuzz2 (pair int) int "dropping matches the list implementation" <|
                \( l, l_ ) n ->
                    drop n l
                        |> Expect.equal (List.drop n l_ |> fromList)
            ]
        , let
            f =
                (-)

            initial =
                0
          in
          describe "folding"
            [ fuzz (pair int) "folding left matches the regular implementation" <|
                \( l, l_ ) ->
                    foldl f initial l
                        |> Expect.equal (List.foldl f initial l_)
            , fuzz (pair int) "folding right matches the regular implementation" <|
                \( l, l_ ) ->
                    foldr f initial l
                        |> Expect.equal (List.foldr f initial l_)
            ]
        , describe "reversing"
            [ fuzz (pair int) "matches the regular implementation" <|
                \( l, l_ ) ->
                    reverse l
                        |> Expect.equal (List.reverse l_ |> fromList)
            ]
        , describe "concatenating"
            [ test "matches the regular implementation" <|
                \_ ->
                    let
                        lists =
                            [ [ 1 ], [ 2 ] ]

                        myLists =
                            fromList <| List.map fromList lists
                    in
                    MyList.concat myLists
                        |> Expect.equal (List.concat lists |> fromList)
            ]
        ]
