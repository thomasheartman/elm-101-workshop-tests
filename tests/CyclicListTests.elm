module CyclicListTests exposing (cycling, higherOrderFunctions, listFunctionality, reversing)

import CyclicList as CyclicList exposing (..)
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, float, int, intRange, list, percentage)
import Test exposing (..)


fuzzElements =
    fuzz3 (list int) int (list int)


cyclicList fuzzer =
    Fuzz.map3 new (list fuzzer) fuzzer (list fuzzer)


getCurrentElement : Test
getCurrentElement =
    describe "current functionality"
        [ fuzzElements "Current returns the current value" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next
                in
                Expect.equal curr (current list)
        , fuzzElements "Current returns the correct value after stepping forward" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next

                    expected =
                        case next of
                            [] ->
                                curr

                            x :: xs ->
                                x
                in
                Expect.equal expected (forward list |> current)
        , fuzzElements "Current returns the correct value after stepping backward" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next

                    expected =
                        case List.reverse prev of
                            [] ->
                                curr

                            x :: xs ->
                                x
                in
                Expect.equal expected (backward list |> current)
        ]


listFunctionality : Test
listFunctionality =
    describe "List functionality"
        [ fuzzElements "It creates correct list representations" <|
            \prev curr next ->
                let
                    a =
                        new prev curr next

                    b =
                        new [] curr (next ++ prev)

                    c =
                        new (next ++ prev) curr []
                in
                ([ curr ] ++ next ++ prev)
                    |> Expect.all
                        [ Expect.equal <| CyclicList.toList a
                        , Expect.equal <| CyclicList.toList b
                        , Expect.equal <| CyclicList.toList c
                        ]
        , fuzz (list int) "it can be created from a list if there is at least one element" <|
            \list ->
                case fromList list of
                    Nothing ->
                        Expect.equal [] list

                    Just _ ->
                        Expect.notEqual [] list
        , fuzz (list int) "the head of the list becomes the current element" <|
            \list ->
                let
                    curr =
                        fromList list
                            |> Maybe.map current
                in
                case list of
                    [] ->
                        Expect.equal Nothing curr

                    x :: xs ->
                        Expect.equal (Just x) curr
        ]


reversing : Test
reversing =
    describe "Reversing"
        [ fuzzElements "twice gives the original order" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next
                in
                Expect.equal list (reverse list |> reverse)
        , fuzzElements "orders items as expected" <|
            \prev curr next ->
                let
                    list =
                        new prev curr next
                in
                Expect.equal (new (List.reverse next) curr (List.reverse prev)) (reverse list)
        ]


cycling : Test
cycling =
    describe "Cycling"
        [ describe "General"
            [ test "it handles a single element" <|
                \_ ->
                    let
                        list =
                            new [] 1 []
                    in
                    list
                        |> Expect.all
                            [ Expect.equal <| forward list
                            , Expect.equal <| backward list
                            ]
            , test "it handles empty previous list" <|
                \_ ->
                    let
                        list =
                            new [] 1 [ 2 ]
                    in
                    new [ 1 ] 2 []
                        |> Expect.all
                            [ Expect.equal <| forward list
                            , Expect.equal <| backward list
                            ]
            , test "it handles empty next list" <|
                \_ ->
                    let
                        list =
                            new [ 1 ] 2 []
                    in
                    new [] 1 [ 2 ]
                        |> Expect.all
                            [ Expect.equal <| forward list
                            , Expect.equal <| backward list
                            ]
            ]
        , describe "It cycles backward"
            [ test "with two populated lists" <|
                \_ ->
                    let
                        list =
                            new [ 1, 2 ] 3 [ 4, 5 ]

                        expected =
                            new [ 1 ] 2 [ 3, 4, 5 ]
                    in
                    Expect.equal expected (backward list)
            , test "into empty list" <|
                \_ ->
                    let
                        list =
                            new [ 1 ] 2 [ 3, 4 ]

                        expected =
                            new [] 1 [ 2, 3, 4 ]
                    in
                    Expect.equal expected (backward list)
            , test "through empty list" <|
                \_ ->
                    let
                        list =
                            new [] 1 [ 2, 3, 4 ]

                        expected =
                            new [ 1, 2, 3 ] 4 []
                    in
                    Expect.equal expected (backward list)
            ]
        , describe "Forward motion"
            [ test "it cycles forward" <|
                \_ ->
                    let
                        list =
                            new [ 1 ] 2 [ 3, 4 ]

                        expected =
                            new [ 1, 2 ] 3 [ 4 ]
                    in
                    Expect.equal expected (forward list)
            , test "into empty list" <|
                \_ ->
                    let
                        list =
                            new [ 1, 2 ] 3 [ 4 ]

                        expected =
                            new [ 1, 2, 3 ] 4 []
                    in
                    Expect.equal expected (forward list)
            , test "through empty list" <|
                \_ ->
                    let
                        list =
                            new [ 1, 2, 3 ] 4 []

                        expected =
                            new [] 1 [ 2, 3, 4 ]
                    in
                    Expect.equal expected (forward list)
            , let
                positiveInt =
                    intRange 0 500

                negativeInt =
                    intRange -500 0

                atIndex i list =
                    case ( i, list ) of
                        ( _, [] ) ->
                            Nothing

                        ( 0, x :: _ ) ->
                            Just x

                        ( _, _ :: xs ) ->
                            atIndex (i - 1) xs
              in
              describe "Multi-step functionality"
                [ fuzz3 (list int) int positiveInt "Test step forward functionality" <|
                    \list curr steps ->
                        let
                            clist =
                                new [] curr list

                            offset =
                                modBy (List.length list + 1) steps

                            expected =
                                Maybe.withDefault curr (atIndex offset ([ curr ] ++ list))
                        in
                        Expect.equal expected (stepForward steps clist |> current)
                , fuzz2 (cyclicList int) negativeInt "Stepping forward (-x) should be the same as stepping backward x" <|
                    \list steps ->
                        Expect.equal (stepBackward (abs steps) list) (stepForward steps list)
                , fuzz2 (cyclicList int) positiveInt "Test step backward functionality" <|
                    \list steps ->
                        Expect.equal (stepForward (negate steps) list |> current) (stepBackward steps list |> current)
                , fuzz2 (cyclicList int) negativeInt "Stepping backward (-x) should be the same as stepping forward x" <|
                    \list steps ->
                        Expect.equal (stepForward (abs steps) list) (stepBackward steps list)
                ]
            ]
        ]


higherOrderFunctions : Test
higherOrderFunctions =
    let
        fuzzArgs =
            fuzz2 (list int) int
    in
    describe "Higher order functions"
        [ fuzzArgs "It maps" <|
            \l x ->
                let
                    list =
                        new [] x l

                    func =
                        (+) 1
                in
                Expect.equal (new [] (func x) (List.map func l)) (map func list)
        , fuzzArgs "it folds right" <|
            \l x ->
                let
                    list =
                        new [] x l

                    func =
                        (-)

                    initial =
                        0
                in
                Expect.equal (List.foldr func initial (x :: l)) (foldr func initial list)
        , fuzzArgs "it folds left" <|
            \l x ->
                let
                    list =
                        new [] x l

                    func =
                        (-)

                    initial =
                        0
                in
                Expect.equal (List.foldl func initial (x :: l)) (foldl func initial list)
        ]
