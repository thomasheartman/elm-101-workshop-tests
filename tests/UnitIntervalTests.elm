module UnitIntervalTests exposing (unitIntervalSuite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, float, percentage)
import Test exposing (..)
import UnitInterval as UnitInterval exposing (..)


expectFloatEqual =
    Expect.within (Expect.Absolute 0.000000000001)


unitIntervalSuite : Test
unitIntervalSuite =
    describe "Testing the UnitInterval works"
        [ describe "Testing output is always between 0 and 1"
            [ fuzz percentage "value is always equal" <|
                \x ->
                    x
                        |> UnitInterval.fromFloat
                        |> UnitInterval.toFloat
                        |> expectFloatEqual x
            , fuzz float "Output is always clamped" <|
                UnitInterval.fromFloat
                    >> UnitInterval.toFloat
                    >> Expect.all [ Expect.atLeast 0, Expect.atMost 1 ]
            ]
        , describe "Scaling"
            [ fuzz2 percentage float "value is always between 0 and 1" <|
                \pct x ->
                    let
                        interval =
                            UnitInterval.fromFloat pct

                        expected =
                            pct * x
                    in
                    UnitInterval.scale interval x
                        |> expectFloatEqual expected
            ]
        ]
