---
separator: <!--  -->
verticalSeparator: <!-- v -->
---

# Elm puzzles

by Thomas Hartmann

[thomashartmann.dev](https://thomashartmann.dev/)

[@hartmann @ mastodon.technology](https://mastodon.technology/@hartmann)

<!--  -->

# What is this?

- A little set of puzzles or tasks to be solved by implementing specific data structures.

- Split into a set of modules with accompanying test modules.

- No UI!

<!-- v -->

**Your goal is to make the tests pass.**

You should not change any function signatures, but are otherwise free to do as you wish.

<!-- v -->

## Purpose

- Exposing limited set of functionality (API design)
- Thinking recursively (functional thinking)
- A peek at typeclasses for the interested parties

note: being able to hide the implementation of some structures has let me implement some of these modules in at least three different ways without changing the tests.

Thinking recursively is something that can be quite challenging for people coming from an imperative background (myself included), but is really nice when you get used to it.

Typeclasses are mostly for my own indulgence, but might be appealing to others too.

<!-- v -->

## Origin

- Previous project (`UnitInteval`, `ZipList`, `CyclicList`)
- Introduction to Elm I did for some coworkers (`MyList`, `Option`)

note: the tasks are based on a combination of data types I've found use for in previous projects of mine, and ideas that have been interesting to implement.

<!--  -->

# Tasks

- 5 modules <!-- .element class="fragment" -->
- 3 categories <!-- .element class="fragment" -->
- All independent <!-- .element class="fragment" -->

note: So here's a quick overview over what the different modules and tasks are. This info is also available in the readme, should you want to see it again. Furthermore, all the source files that require implementation are commented and should (hopefully) be enough on their own.

<!-- v -->

## What do they look like?

Unimplemented types:

```elm
type UnitInterval
    = UnitInterval
```

Unimplemented functions:

```elm
fromFloat : Float -> UnitInterval
fromFloat _ =
    Debug.todo "fromFloat"
```

note:
The underscore stands in for the input variables. You may write as verbosely or tersely as you wish, using as many of the arguments as you please.

<!-- v -->

## Unit Interval

- A value between 0 and 1.

- A simple, opaque data type that exposes a limited set of functions to work with it.

### Goals

- Implement the data type
- Implement the functions

note: the data type is a placeholder single-constructor custom data type. You will need to decide how you want to implement it internally.

<!-- v -->

## Non-empty lists

**`ZipList`** and **`CyclicList`** are both _non-empty lists_, though with slightly different specifications and behaviour.

### Goals

- Implement the data types
- Implement the functions

note: as with the unit interval, you'll need to decide how you want to represent these structures internally.

<!-- v -->

### `ZipList`

Non-empty list with 'current' element <!-- .element class="fragment" -->

Can move backwards and forwards, but only to either end <!-- .element class="fragment" -->

note: A non-empty list that has a notion of a 'current' element. You can move backwards and forwards through the list, but only to either end.

<!-- v -->

### `CyclicList`

Non-empty list with 'current' element <!-- .element class="fragment" -->

Can move backwards and forwards, loops around when it reaches the end <!-- .element class="fragment" -->

note:
Another non-empty list with a notion of a 'current' element and that you can move through in either direction.

Different from the `ZipList`, though, this list doesn't have a notion of a beginning or an end. If it were to reach the end of its elements, it should loop back around and start again.

<!-- v -->

## `Option` and `MyList`

Re-implementations of common Elm data types <!-- .element class="fragment" -->

Data type is already taken care of <!-- .element class="fragment" -->

Try and avoid using functions from their 'official' types <!-- .element class="fragment" -->

Common functionality plus some typeclass goodness <!-- .element class="fragment" -->

Please note that the tests for these modules will not run properly until you implement the 'mempty' value. <!-- .element class="fragment" -->

note:
`Option` and `MyList` are both re-implementations of common Elm data types; `Maybe` and `List` respectively.

For these types, their representation is already there, so your task is to implement some standard functions on these types and some additional functionality based on some Haskell typeclasses.

Presumably because of how Elm-test (or Elm) works, these modules will crash when you try and run the tests with a nasty JS stacktrace. This is because everything, including the constant values have been implemented using `Debug.todo`, and doing that on constant values is bad.

### Goals

- Implement the functions

<!-- v -->

```sh
throw new Error('TODO in module `' + moduleName + '` ' + _Debug_regionToString(region) + '\n\n' + message);
^

Error: TODO in module `MyList` on line 192
mempty
    at _Debug_crash (/long/path/elmTestOutput.js:806:10)
```

<!--  -->

# Err ... what's with the Haskell?

<!-- v -->

**DISCLAIMER**: I AM NOT AN EXPERT

<!-- v -->

Currently learning Haskell

This was a way for me to try and represent the ideas in a language that doesn't have typeclasses <!-- .element class="fragment" -->

Allows me to focus on what the typeclasses actually achieve for specific instances <!-- .element class="fragment" -->

**I found it interesting and enjoyable and thought the rest of you might as well** <!-- .element class="fragment" -->

note: Elm was my first functional language and worked as a gateway drug, leading me to Haskell. Now I'm really getting into the latter and these are some pretty cool typeclasses to work with.

Also, when deciding to implement these functions, I found that most of them are already defined on the types in question, just under different names.

<!-- v -->

_Okay, so what are the typeclasses in question?_

<!-- v -->

# Monoid

A type that satisfies the following conditions:

Can be combined ('appended') to other values of the same type <!-- .element class="fragment" -->

Has an 'empty' value, i.e. one that can be combined with another value without changing the other value <!-- .element class="fragment" -->

Combining these values is an associative operation <!-- .element class="fragment" -->

note:
Take numbers under addition.
Appending is the `plus` function
The empty value is `0`
`1 + (2 + 3)` is equal to `(1 + 2) + 3`.

<!-- v -->

# Functor

Something that can be 'mapped' over.

Defines a single function: `fmap`.

note:
For lists, this is trivial. It's simply the `map` function. For most other datatypes that define a `map` function in Elm, this is the same functionality.

<!-- v -->

# Monad 😱

Less scary than you'd think. In our case, we're interested in _two_ pieces of functionality:

A way to put an element in your monad. Think of it like a constructor. <!-- .element class="fragment" -->

A way to chain functions that operate on naked values and produce monads. <!-- .element class="fragment" -->

note: the big scary word! Not so scary after all.
The way to put the element in your monad is known as `return`

The way to chain functions is known as `bind`

<!--  -->

The repo is available at

<https://gitlab.com/t-hart/elm-101-workshop-tests/>

Any questions, mistakes, clarifications, etc: _let me know!_

Tip: focus on one module at a time!
