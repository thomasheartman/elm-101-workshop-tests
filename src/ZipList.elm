module ZipList exposing
    ( backward
    , current
    , end
    , fastForward
    , foldl
    , foldr
    , forward
    , fromList
    , map
    , new
    , reverse
    , rewind
    , start
    , stepBackward
    , stepForward
    , toList
    )

{-
   A non-empty list that has a beginning and an end, and that keeps track of the current element.
   Useful for working with a list of elements where you want to track what was before and what comes
   next.

   You are free to use any internal representation you want, as long as it matches
   the exposed API and doesn't require any changes to the tests.
-}
{-
   The type declaration. The constructors are hidden. You'll need to figure out
   how you want to represent the type internally and implement that.
-}


type ZipList a
    = ZipList



{-
   The only way to create a zip list from the outside.
   Takes three arguments:
   - the previous elements in the list
   - the current element in the list
   - the next elements in the list
   And produces a ZipList based on these inputs.
-}


new : List a -> a -> List a -> ZipList a
new _ _ _ =
    Debug.todo "new"



{-
   Get the 'current' value from a zip list. The easiest way to get the currently
   selected item.
-}


current : ZipList a -> a
current _  =
    Debug.todo "current"



{-
   Turn a zip list into a List, preserving the order of the zip list.
-}


toList : ZipList a -> List a
toList _ =
    Debug.todo "toList"



{-
   Given a list, try and create a Zip List from it.
   Because a list can be empty and a zip list cannot,
   this function needs to take that into account.
-}


fromList : List a -> Maybe (ZipList a)
fromList _ =
    Debug.todo "fromList"


{-
   Reverse a zip list. The current element should
   still be the current element after reversal.
-}


reverse : ZipList a -> ZipList a
reverse _ =
    Debug.todo "reverse"



{-
   Step forward through the list by one step. If we're at the end of the list,
   do nothing.
-}


forward : ZipList a -> ZipList a
forward _ =
    Debug.todo "forward"


{-
   Step backward through the list by one step. If we're at the start of the list, do nothing.
-}


backward : ZipList a -> ZipList a
backward _ =
    Debug.todo "backward"



{- Go to the very start of the list -}


rewind : ZipList a -> ZipList a
rewind _ =
    Debug.todo "rewind"


{- Go to the end of a list -}


fastForward : ZipList a -> ZipList a
fastForward _ =
    Debug.todo "fastForward"

{-
   Step forward n steps. If given a negative step value, should step
   backward instead. Stop stepping when there are no more steps to take
   or you're at the start/end.
-}


stepForward : Int -> ZipList a -> ZipList a
stepForward _ _ =
    Debug.todo "stepForward"



{-
   Step backward n steps. If given a negative step value, should step
   forward instead. Stop stepping when there are no more steps to take
   or you're at the start/end.
-}


stepBackward : Int -> ZipList a -> ZipList a
stepBackward _ _ =
    Debug.todo "stepBackward"



{- Is the given zip list at its first element? -}


start : ZipList a -> Bool
start _ =
    Debug.todo "start"



{- Is the given zip list at its last element? -}


end : ZipList a -> Bool
end _ =
    Debug.todo "end"



{-
   The foldr function. Should fold the list in the same way as a
   List.foldr would a list
-}


foldr : (a -> b -> b) -> b -> ZipList a -> b
foldr _ _ _=
    Debug.todo "foldr"



{-
   The foldl function. Should fold the list in the same way as a
   List.foldl would a list
-}


foldl : (a -> b -> b) -> b -> ZipList a -> b
foldl _ _ _ =
    Debug.todo "foldl"



{-
   The map function. Equivalent to the List.map function,
   it should change the values in a zip list from type
   a to type b given the provided function.
-}


map : (a -> b) -> ZipList a -> ZipList b
map _ _=
    Debug.todo "map"
