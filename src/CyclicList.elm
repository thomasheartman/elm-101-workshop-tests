module CyclicList exposing
    ( CyclicList
    , backward
    , current
    , foldl
    , foldr
    , forward
    , fromList
    , map
    , new
    , reverse
    , stepBackward
    , stepForward
    , toList
    )

{-
   A non-empty list that loops around on itself.
   Useful for working with any sort of data where it might be useful to cycle
   around to the start, such as with a collection of themes for a website or
   editor, or spells in a video game. Could also be used to model a clock.

   You are free to use any internal representation you want, as long as it matches
   the exposed API and doesn't require any changes to the tests.
-}
{-
   the type declaration. The constructors are hidden. You'll need to figure out
   how you want to represent the list internally and implement that.
-}


type CyclicList a
    = CyclicList



{-
   The only way to create a cyclic list from the outside.
   Takes three arguments:
   - the previous elements in the list
   - the current element in the list
   - the next elements in the list
   And produces a CyclicList based on these inputs.
-}


new : List a -> a -> List a -> CyclicList a
new prev curr next =
    Debug.todo "new"



{-
   Get the 'current' value from a cyclic list. The easiest way to get the currently
   selected item.
-}


current : CyclicList a -> a
current _ =
    Debug.todo "current"



{-
   Turn a cyclic list into a List. The current element
   should be the head of the list, with the remaining
   elements following on as they would if you were
   to step through it in a normal fashion.
-}


toList : CyclicList a -> List a
toList _ =
    Debug.todo "toList"



{-
   Given a list, try and create a Cyclic List from it.
   Because a list can be empty and a cyclic list cannot,
   this function needs to take that into account.
-}


fromList : List a -> Maybe (CyclicList a)
fromList _ =
    Debug.todo "fromList"



{-
   Reverse a cyclic list. The current element should
   still be the current element after reversal.
-}


reverse : CyclicList a -> CyclicList a
reverse _ =
    Debug.todo "reverse"



{-
   Step forward through the list by one step.
-}


forward : CyclicList a -> CyclicList a
forward _ =
    Debug.todo "forward"



{-
   Step backward through the list by one step.
-}


backward : CyclicList a -> CyclicList a
backward _ =
    Debug.todo "backward"



{-

   Step forward n steps. If given a negative step value, should step
   backward instead.
-}


stepForward : Int -> CyclicList a -> CyclicList a
stepForward _ _ =
    Debug.todo "stepForward"



{-

   Step backward n steps. If given a negative step value, should step
   forward instead.
-}


stepBackward : Int -> CyclicList a -> CyclicList a
stepBackward _ _ =
    Debug.todo "stepBackward"



{-
   The foldr function. Should fold the list in the same way as a
   List.foldr would a list, assuming that the current element is
   the head of the list.
-}


foldr : (a -> b -> b) -> b -> CyclicList a -> b
foldr _ _ _ =
    Debug.todo "foldr"



{-
   The foldl function. Should fold the list in the same way as a
   List.foldl would a list, assuming that the current element is
   the head of the list.
-}


foldl : (a -> b -> b) -> b -> CyclicList a -> b
foldl _ _ _ =
    Debug.todo "foldl"



{-

   The map function. Equivalent to the List.map function,
   it should change the values in a cyclic list from type
   a to type b given the provided function.
-}


map : (a -> b) -> CyclicList a -> CyclicList b
map _ _ =
    Debug.todo "map"
