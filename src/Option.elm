module Option exposing (Option(..), bind, fmap, mappend, mempty, return, withDefault)

-- A reimplementation of the Maybe type, exposing functions for use as a
-- Monoid, a Functor, and a Monad.

-- NOTE: THIS MODULE'S TESTS WILL NOT RUN PROPERLY UNTIL YOU IMPLEMENT THE `MEMPTY` VALUE BELOW.

type Option a
    = Some a
    | None



-- return the value contained in the Option or the provided default value
-- if the Option is None.


withDefault : a -> Option a -> a
withDefault _ _ =
    Debug.todo "withDefault"



{-
   monoid functions:
   mempty : a
   mempty: returns the empty element, that which can be combined with any other element of the monoid and still return the original one

   mappend : a -> a -> a
   mappend: associatively concat two monoids

   To be able to implement `mappend` the way it exists in Haskell, we would need `a` to be an instance of the typeclass ~semigroup~,
   but because we cannot specify that in elm, we'll need an extra function telling us how to combine two `a`s.

   the signature therefore becomes (for Option):
   mappend : (a -> a -> a) -> Option a -> Option a -> Option a
-}
-- The empty element.


mempty : Option a
mempty =
    Debug.todo "mempty"



-- Append (mappend) to Options using the function provided to operate on the contained value.
-- In the event that one of the Options is None, return the other Option.
--
-- Examples:
-- `mappend (+) (Some 2) None` should yield 2
-- `mappend (+) None None` should yield None
-- `mappend (+) (Some 2) (Some 3)` should yield 5


mappend : (a -> a -> a) -> Option a -> Option a -> Option a
mappend _ _ _=
    Debug.todo "mappend"


-- Map a function over the value in an option if there is one.
-- Equivalent to Maybe.map
--
{--functor functions--}


fmap : (a -> b) -> Option a -> Option b
fmap _ _=
    Debug.todo "fmap"


--
{--monad functions--}
-- Wrap the given value in an Option


return : a -> Option a
return _ =
    Debug.todo "return"



-- Given a function from a to Option b and an Option a,
-- apply the function on the value if it exists.
--
-- Equivalent to Maybe.andThen


bind : (a -> Option b) -> Option a -> Option b
bind _ _ =
    Debug.todo "bind"
