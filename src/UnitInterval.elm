module UnitInterval exposing (UnitInterval, fromFloat, scale, toFloat)

{-
   A unit interval is a floating point number between 0 and 1, commonly used to
   indicate a percentage or other values that go from 'nothing' to 'complete'.

   This module exposes an opaque data type and some basic functionality to work
   with said type.

   Your job is to define the data type and then to implement the functions for it.
-}
--
{-
   The data type in question. Unimplemented, this is just a custom type with one
   constructor. You are free to add as many or as few constructors as you want as
   long as they let you remember the value supplied when creating an interval.
-}


type UnitInterval
    = UnitInterval



{-
   The only way to construct a Unit Interval exposed to the outside world.
   The function should take a floating point number and return a unit interval
   based on the input. If the input given is outside the range of [0, 1] (inclusive),
   then the value should be clamped to the closest possible value within the valid range.
-}


fromFloat : Float -> UnitInterval
fromFloat _ =
    Debug.todo "fromFloat"



{-
   The only way to  convert a Unit Interval to a floating point number.
   Given a Unit Interval, return the value that is being contained as
   a floating point number.
-}


toFloat : UnitInterval -> Float
toFloat _ =
    Debug.todo "toFloat"



{-
   Scale a floating point number by a unit interval.
   Given a Unit Interval and a float, return the value the value that would result
   from multiplying the float with the unit interval.

   Example: given a Unit Interval equal to 0.5 and a Float equal to 10, return 5.
-}


scale : UnitInterval -> Float -> Float
scale _ _ =
    Debug.todo "scale"
