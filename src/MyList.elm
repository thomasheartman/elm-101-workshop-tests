module MyList exposing
    ( MyList(..)
    , bind
    , concat
    , drop
    , filter
    , fmap
    , fmapAlways
    , foldl
    , foldr
    , fromList
    , length
    , map
    , mappend
    , member
    , mempty
    , return
    , reverse
    , take
    , toList
    )

-- A singly-linked list, a reimplementation of the standard List data type.
-- This module exposes a subset of the most common list operations as well
-- as certain functionality based on the Haskell typeclasses 'Monoid' 'Functor' and 'Monad'
--
-- For more information on the typeclasses, see the readme.
--
-- You should not change the type definition in this module, only implement the functions.
-- You are also strongly encouraged to implement all the functionality in this module without
-- using the standard List functions (and converting back and forth).

-- NOTE: THIS MODULE'S TESTS WILL NOT RUN PROPERLY UNTIL YOU IMPLEMENT THE `MEMPTY` VALUE BELOW.


type MyList a
    = Nil
    | Cons a (MyList a)



-- Turn a standard List into a MyList


fromList : List a -> MyList a
fromList _ =
    Debug.todo "fromList"



-- Turn a MyList into a List


toList : MyList a -> List a
toList _ =
    Debug.todo "toList"



-- Regular list functionality.
-- For more information about how these functions should behave,
-- check out the documentation for the core List module.
--
-- Map over a list. Given a function (a -> b) and a MyList a,
-- apply the function to every element in the list and return the
-- resulting MyList b


map : (a -> b) -> MyList a -> MyList b
map _ _ =
    Debug.todo "map"



-- Return the number of elements in a MyList


length : MyList a -> Int
length _ =
    Debug.todo "length"



-- Does the given element exist in the list.


member : a -> MyList a -> Bool
member _ _ =
    Debug.todo "member"



-- take n elements from the head of a list


take : Int -> MyList a -> MyList a
take _ _ =
    Debug.todo "take"



-- drop n elements from the start of a list


drop : Int -> MyList a -> MyList a
drop _ _ =
    Debug.todo "drop"



-- foldr over a list. Equivalent to the List.foldr for List.


foldr : (a -> b -> b) -> b -> MyList a -> b
foldr _ _ _ =
    Debug.todo "foldr"



-- foldl over a list. Equivalent to the List.foldr for List.


foldl : (a -> b -> b) -> b -> MyList a -> b
foldl _ _ _ =
    Debug.todo "foldl"



-- filter a MyList. Given a predicate, return
-- a MyList where only the elements satisfying
-- the predicate remain.


filter : (a -> Bool) -> MyList a -> MyList a
filter _ _ =
    Debug.todo "filter"



-- Concatenate a MyList (MyList a) into a single MyList a.
-- Works like flattening a list of lists into a single list.


concat : MyList (MyList a) -> MyList a
concat _ =
    Debug.todo "concat"



-- reverse a MyList


reverse : MyList a -> MyList a
reverse _ =
    Debug.todo "reverse"



{--
# Typeclass territory


## monoid functions:
mempty : a
mempty: returns the empty element, that which can be combined with any other element of the monoid and still return the original one

mappend : a -> a -> a
mappend: associatively concat two monoids


## functor functions:
map : mapping a function over

## monad functions:

return :: Monad m => a -> m a
`return` takes a value and puts it in a monadic context, wraps it in your monad, so to speak.

(>>=) :: Monad m => m a -> (a -> m b) -> m b
`>>=` (pronounced 'bind') takes a Monad of `a`, a function from `a` to `b` and returns a Monad of `b`
`bind` is an infix operator in Haskell, but because Elm no longer allows for custom operators or infix
functions, it makes more sense to flip it, such that the function is first, to allow for piping

--}
-- Monoidal  functions
--
-- The empty element


mempty : MyList a
mempty =
    Debug.todo "mempty"



-- Appending (mappending): putting two lists together.
-- The equivalent of the (++) operator for normal lists.


mappend : MyList a -> MyList a -> MyList a
mappend _ _ =
    Debug.todo "mappend"



-- {--impl functor for list --}
-- map over a functor. For the case of a List (or MyList),
-- this is the exact equivalent of the map function.


fmap : (a -> b) -> MyList a -> MyList b
fmap _ _ =
    Debug.todo "fmap"



-- Given a value b, replace all the values in the MyList
-- with the given value.


fmapAlways : b -> MyList a -> MyList b
fmapAlways _ _ =
    Debug.todo "fmapAlways"



{--impl Monad for list --}
--
-- Given an element, put it in a MyList context
-- (create a MyList with that element)


return : a -> MyList a
return _ =
    Debug.todo "return"



-- Given a function from a to MyList b and
-- a MyList a, return the concatenated results
-- of the function applied to all elements in
-- the MyList.
--
-- Equivalent to the List.concatMap function
-- in the core library.


bind : (a -> MyList b) -> MyList a -> MyList b
bind _ _ =
    Debug.todo "bind"
